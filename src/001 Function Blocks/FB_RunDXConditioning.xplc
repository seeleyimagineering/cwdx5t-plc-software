<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<functionBlock name="FB_RunDXConditioning" version="1.0.0" creationDate="1698964982" lastModifiedDate="1729115542">
	<vars>
		<inputVars>
			<var name="pDX" type="@FB_DX"/>
			<var name="xMode" type="BOOL"/>
			<var name="iSETdb" type="INT"/>
			<var name="iRMdb" type="INT"/>
			<var name="iSAdb" type="INT"/>
			<var name="iOAdb" type="INT"/>
			<var name="xTempering" type="BOOL"/>
		</inputVars>
		<externalVars>
			<var name="E2_iDXHeatCompressorDeadband" type="INT"/>
			<var name="E2_iDXCoolCompressorDeadband" type="INT"/>
			<var name="gk_xHeat" type="BOOL"/>
			<var name="gk_xCool" type="BOOL"/>
			<var name="E2_iOAdbCoolLockout" type="INT"/>
			<var name="E2_iOAdbHeatLockout" type="INT"/>
			<var name="E2_iSAdbHeatLockout" type="INT"/>
			<var name="E2_iSAdbCoolLockout" type="INT"/>
			<var name="E2_xOAdbHeatLockoutEnabled" type="BOOL"/>
			<var name="E2_xOAdbCoolLockoutEnabled" type="BOOL"/>
			<var name="E2_xSAdbHeatLockoutEnabled" type="BOOL"/>
			<var name="E2_xSAdbCoolLockoutEnabled" type="BOOL"/>
			<var name="E2_uiHeatTemperingDXSFSpeed" type="UINT"/>
			<var name="sMsg" type="STRING" length="128"/>
			<var name="E2_uiCoolTemperingDXSFSpeed" type="UINT"/>
		</externalVars>
		<localVars>
			<var name="srCooling" type="SR"/>
			<var name="srHeating" type="SR"/>
			<var name="srStageControl" type="SR"/>
			<var name="diDXControl" type="DINT">
				<initValue>DXControl#_Invalid</initValue>
			</var>
			<var name="diDXControl_old" type="DINT">
				<initValue>DXControl#_Invalid</initValue>
			</var>
			<var name="xRet" type="BOOL"/>
		</localVars>
		<localConsts>
			<const name="iSAdbHeatingLowStageThreshold" type="INT">
				<initValue>320</initValue>
			</const>
			<const name="iSAdbHeatingHighStageThreshold" type="INT">
				<initValue>360</initValue>
			</const>
			<const name="iSAdbCoolingHighStageThreshold" type="INT">
				<initValue>25</initValue>
			</const>
			<const name="iSAdbCoolingLowStageThreshold" type="INT">
				<initValue>20</initValue>
			</const>
		</localConsts>
	</vars>
	<iecDeclaration active="FALSE"/>
	<interfaces/>
	<methods/>
	<sourceCode type="ST">
		<![CDATA[
srCooling.s1 := (iRMdb >= iSETdb + E2_iDXCoolCompressorDeadband);
srCooling.r := (iRMdb <= iSETdb - E2_iDXCoolCompressorDeadband);
srCooling();

srHeating.s1 := (iRMdb <= iSETdb - E2_iDXHeatCompressorDeadband);
srHeating.r := (iRMdb >= iSETdb + E2_iDXHeatCompressorDeadband);
srHeating();

@pDX.i_xMode := FALSE;
@pDX.i_xHeatOrCool := xMode;
@pDX.i_uiSupplyFanCtrl := 0;

IF (xMode = gk_xHeat) THEN;

	srStageControl.s1 := iSAdb <= iSAdbHeatingLowStageThreshold;
	srStageControl.r := iSAdb >= iSAdbHeatingHighStageThreshold;
	srStageControl();

	@pDX.i_xMode := FALSE;
	
	IF ((E2_xOAdbHeatLockoutEnabled) AND (iOAdb >= E2_iOAdbHeatLockout)) THEN;
	
		diDXControl := DXControl#HeatOAdbLockOut;
	
		@pDX.i_diCompressorCtrl := CompressorStage#Off;
		@pDX.i_uiSupplyFanCtrl := SEL(xTempering, 0, gk_uiDXSFSpeedIdle); 	
		
	ELSIF ((E2_xSAdbHeatLockoutEnabled) AND (iSAdb >= E2_iSAdbHeatLockout)) THEN;
		
		diDXControl := DXControl#HeatSAdbLockOut;
	
		@pDX.i_diCompressorCtrl := CompressorStage#Off;
		@pDX.i_uiSupplyFanCtrl := SEL(xTempering, 0, gk_uiDXSFSpeedIdle);
		
	ELSIF (srHeating.q1) THEN;
	
		diDXControl := DXControl#HeatRun;
	
		@pDX.i_diCompressorCtrl := SEL(srStageControl.q1, CompressorStage#Low, CompressorStage#High);
		@pDX.i_uiSupplyFanCtrl := 0;
		(* we are allowing the fan speed to managed against the stage *)
				
	ELSIF (xTempering) THEN;
		
		diDXControl := DXControl#HeatTempering;
	
		@pDX.i_diCompressorCtrl := CompressorStage#Off;
		@pDX.i_uiSupplyFanCtrl := E2_uiHeatTemperingDXSFSpeed;
		
	ELSE
	
		diDXControl := DXControl#HeatOff;
		
		@pDX.i_diCompressorCtrl := CompressorStage#Off;
		@pDX.i_uiSupplyFanCtrl := 0;
	
	END_IF;

ELSIF (xMode = gk_xCool) THEN;

	srStageControl.s1 := iSAdb >= iSAdbCoolingHighStageThreshold;
	srStageControl.r := iSAdb <= iSAdbCoolingLowStageThreshold;
	srStageControl();

	@pDX.i_xMode := FALSE;
	
	IF ((E2_xOAdbCoolLockoutEnabled) AND (iOAdb >= E2_iOAdbCoolLockout)) THEN;
	
		diDXControl := DXControl#CoolOAdbLockOut;
	
		@pDX.i_diCompressorCtrl := CompressorStage#Off;
		@pDX.i_uiSupplyFanCtrl := SEL(xTempering, 0, gk_uiDXSFSpeedIdle); 
		
	ELSIF ((E2_xSAdbCoolLockoutEnabled) AND (iSAdb >= E2_iSAdbCoolLockout)) THEN;
		
		diDXControl := DXControl#CoolSAdbLockOut;
	
		@pDX.i_diCompressorCtrl := CompressorStage#Off;
		@pDX.i_uiSupplyFanCtrl := SEL(xTempering, 0, gk_uiDXSFSpeedIdle);
		
	ELSIF (srCooling.q1) THEN;
	
		diDXControl := DXControl#CoolRun;
	
		@pDX.i_diCompressorCtrl := SEL(srStageControl.q1, CompressorStage#Low, CompressorStage#High);
		@pDX.i_uiSupplyFanCtrl := 0;
		(* we are allowing the fan speed to managed against the stage *)
				
	ELSIF (xTempering) THEN;
	
		diDXControl := DXControl#CoolTempering;
	
		@pDX.i_diCompressorCtrl := CompressorStage#Off;
		@pDX.i_uiSupplyFanCtrl := E2_uiCoolTemperingDXSFSpeed;
		
	ELSE
	
		diDXControl := DXControl#CoolOff;
		
		@pDX.i_diCompressorCtrl := CompressorStage#Off;
		@pDX.i_uiSupplyFanCtrl := 0;
	
	END_IF;
	
END_IF;

{ IFDEF : LOG_LEVEL_INFO }
	IF (diDXControl <> diDXControl_old) THEN;
		sMsg := CONCAT('DX control: ', TO_STRING(diDXControl_old));
		sMsg := CONCAT(sMsg, ' -> ');
		sMsg := CONCAT(sMsg, TO_STRING(diDXControl));
		xRet := Log_Info(sMsg);
	END_IF;		
{ ENDIF } // LOG_LEVEL_INFO
diDXControl_old := diDXControl;
			
]]>
	</sourceCode>
</functionBlock>