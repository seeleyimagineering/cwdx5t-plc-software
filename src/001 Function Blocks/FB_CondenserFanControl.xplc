<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<functionBlock name="FB_CondenserFanControl" version="1.0.0" creationDate="1692572749" lastModifiedDate="1692584199">
	<vars>
		<inputVars>
			<var name="xEn" type="BOOL">
				<initValue>FALSE</initValue>
			</var>
			<var name="iDischargePs" type="INT"/>
			<var name="iSuctionPs" type="INT"/>
			<var name="xSensorFault" type="BOOL">
				<initValue>FALSE</initValue>
			</var>
			<var name="uiMinSpeed" type="UINT">
				<initValue>0</initValue>
			</var>
			<var name="uiMaxSpeed" type="UINT">
				<initValue>100</initValue>
			</var>
			<var name="iOAdb" type="INT">
				<initValue>0</initValue>
			</var>
			<var name="xCompressorState" type="BOOL">
				<initValue>FALSE</initValue>
			</var>
		</inputVars>
		<outputVars>
			<var name="uiSpeed" type="UINT"/>
		</outputVars>
		<localVars>
			<var name="ControlDischargePs" type="FloatingHighPresCntrl"/>
			<var name="ControlSuctionPs" type="EvapPressCntrl"/>
			<var name="xInit" type="BOOL">
				<initValue>FALSE</initValue>
			</var>
		</localVars>
		<localConsts>
			<const name="iDXCondenserMinDb" type="INT">
				<descr>Minimum condensing temperature.</descr>
				<initValue>250</initValue>
			</const>
			<const name="iDXCondenserMaxDb" type="INT">
				<descr>Maximum condensing temperature.</descr>
				<initValue>650</initValue>
			</const>
			<const name="usiRefID410A" type="USINT">
				<descr>ID of the refrigerant used in the DX cycle. 4 = R410A.</descr>
				<initValue>4</initValue>
			</const>
			<const name="uiCondPIDIntegralTC" type="UINT">
				<descr>PID Integral time constant for high pressure control under cooling operation.</descr>
				<initValue>50</initValue>
			</const>
			<const name="uiCondPIDGain" type="UINT">
				<descr>PID Gain for high pressure control under cooling operation.</descr>
				<initValue>5</initValue>
			</const>
			<const name="usiControlModeFloating" type="USINT">
				<descr>ID for pressure control mode. 0 = floating.</descr>
				<initValue>0</initValue>
			</const>
			<const name="iCondPressMaxLimit" type="INT">
				<descr>Maximum allowable pressure under cooling operation.</descr>
				<initValue>3700</initValue>
			</const>
			<const name="xCelsius" type="BOOL">
				<descr>ID for Celsius units.</descr>
				<initValue>FALSE</initValue>
			</const>
			<const name="xBar" type="BOOL">
				<descr>ID for bar units.</descr>
				<initValue>FALSE</initValue>
			</const>
			<const name="uiEvapPIDGain" type="UINT">
				<descr>PID Gain for evaporation control.</descr>
				<initValue>5</initValue>
			</const>
			<const name="uiEvapPIDIntegralTC" type="UINT">
				<descr>PID Integral time constant for evaporation control.</descr>
				<initValue>50</initValue>
			</const>
			<const name="iEvapTempOffset" type="INT">
				<descr>Evaporating temperature offset.</descr>
				<initValue>-100</initValue>
			</const>
			<const name="iEvapTempOffsetAtMax" type="INT">
				<descr>Evaporating temperature offset at maximum setpoint.</descr>
				<initValue>-200</initValue>
			</const>
			<const name="iEvapTempOffsetAtMin" type="INT">
				<descr>Evaporating temperature offset at minimum setpoint.</descr>
				<initValue>-150</initValue>
			</const>
			<const name="iEvapTempMin" type="INT">
				<descr>Minimum evaporating temperature.</descr>
				<initValue>-120</initValue>
			</const>
			<const name="iEvapTempMax" type="INT">
				<descr>Maximum evaporating temperature.</descr>
				<initValue>120</initValue>
			</const>
			<const name="iEvapTempExtSetp" type="INT">
				<initValue>0</initValue>
			</const>
		</localConsts>
	</vars>
	<iecDeclaration active="FALSE"/>
	<interfaces/>
	<methods/>
	<sourceCode type="ST">
		<![CDATA[IF NOT xInit THEN;

	ControlDischargePs.iCondTempMin := iCondTempMin;
	ControlDischargePs.iCondTempMax := iCondTempMax;
	ControlDischargePs.usiRefType := gk_usiRefrR410A;
	ControlDischargePs.uiKp := uiCondPIDGain;
	ControlDischargePs.uiTi := uiCondPIDIntegralTC;
	ControlDischargePs.usiControlMode := usiControlModeFloating;
	ControlDischargePs.iCondPressMaxLimit := iCondPressMaxLimit;
	ControlDischargePs.xUnitTypeTemp := xCelsius;
	ControlDischargePs.xUnitTypePress := xBar;

	ControlSuctionPs.iEvapTempExtSetp := iEvapTempExtSetp;
	ControlSuctionPs.iEvapTempOffset := iEvapTempOffset;
	ControlSuctionPs.iEvapTempOffsetAtMin := iEvapTempOffsetAtMin;
	ControlSuctionPs.iEvapTempOffsetAtMax := iEvapTempOffsetAtMin;
	ControlSuctionPs.iEvapTempMin := iEvapTempMin;
	ControlSuctionPs.iEvapTempMax := iEvapTempMax;
	ControlSuctionPs.usiRefType := gk_usiRefrR410A;
	ControlSuctionPs.uiKp := uiEvapPIDGain;
	ControlSuctionPs.uiTi := uiEvapPIDIntegralTC;
	ControlSuctionPs.usiControlMode := usiControlModeFloating;
	ControlSuctionPs.iEvapPressMinLimit;
	ControlSuctionPs.xUnitTypeTemp := xCelsius;
	ControlSuctionPs.xUnitTypePress	:= xBar;
	
	xInit := TRUE;

ELSIF NOT xEnable THEN;

	uiSpeed := 0;

ELSE

	IF NOT xCompressorState THEN;
	
		uiSpeed := uiMinSpeed;
	
	ELSIF xSensorFault THEN;
	
		uiSpeed := uiMaxSpeed;		

	ELSIF xMode = xHeat THEN;
	
		ControlDischargePs.xEn := TRUE;
		ControlDischargePs.iCondPress := iDischargePs;
		ControlDischargePs.iOutAirTemp := iOAdb;
		
		ControlDischargePs();
		
		uiSpeed := ControlDischargePs.uiFanCntrlSignal;

		
	ELSE
	
		ControlSuctionPs.xEn := TRUE;
		ControlSuctionPs.iEvapPress := iSuctionPs;
		ControlSuctionPs.iOutAirTemp := iOAdb;
		
		ControlSuctionPs();
		
		uiSpeed := ControlSuctionPs.uiFanCntrlSignal;
		
	END_IF;

END_IF;





(* we take heat or cool signal, ambient fault *)
]]>
	</sourceCode>
</functionBlock>